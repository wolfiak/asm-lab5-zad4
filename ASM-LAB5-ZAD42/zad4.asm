.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11
includelib C:\Users\pacio\Downloads\masm\masm32.lib
GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
atoi  PROTO :DWORD
lstrlenA PROTO :DWORD
GetTickCount PROTO 
nseed PROTO :DWORD 
nrandom PROTO :DWORD
.DATA
		cout		   dd ?
		cin			   dd ?
		tekst          db "Wprowadz liczbe: ",0
		rozmiart       db $ - tekst
		liczba         dd ?
	    liczbaW		   db "Wynik  to: %i ",10,0
		rliczbaW	   dd $ - liczbaW
		bufor          db 128 DUP(?)
		bufor2         db 10 DUP(?)
		tekst2         db "Wproawdz liczbe: ",0
		rozmiart2       dd $ - tekst2
		komunikat      db "Liczba ktora podales jest wieksza :( Podaj mniejsza liczbe",10,0
		rkomunikat     dd $ - komunikat
		komunikat2     db "Liczba ktora podales jest mniejsza :(  Podaj wieksza liczbe",10,0
		rkomunikat2    dd $ - komunikat2
	
		range		   db ?
		wylosowana	   db 1
		dupa		   db 2
		liczbaZ        dd 0
		rozmiar		   dd ?

		
.CODE
main proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX
	
	invoke WriteConsoleA, cout, OFFSET tekst, rozmiart, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor2, 8, OFFSET liczbaZ, 0
	lea EBX, bufor2
	mov EDI, liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor2
	mov range, AL



	invoke GetTickCount
	invoke nseed, EAX
	invoke nrandom, range
	mov wylosowana, AL
	;invoke wsprintfA, OFFSET bufor, OFFSET liczbaW, wylosowana
;	invoke WriteConsoleA, cout, OFFSET bufor, rliczbaW, OFFSET liczba, 0
	Pentela:
	invoke WriteConsoleA, cout, OFFSET tekst2, rozmiart2, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor2, 8, OFFSET liczbaZ,0
	lea EBX, bufor2
	mov EDI, liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor2
;	mov liczba, AL

	cmp AL, wylosowana
	je Koniec
	jg Duzo
	invoke WriteConsoleA, cout, OFFSET komunikat2, rkomunikat2, OFFSET liczba,0
	Pentluj:
	jmp Pentela
	Koniec:

	invoke wsprintfA, OFFSET bufor, OFFSET liczbaW, wylosowana
	invoke WriteConsoleA, cout, OFFSET bufor, rliczbaW, OFFSET liczba, 0

invoke ExitProcess, 0
	
	Duzo:
		invoke WriteConsoleA, cout, OFFSET komunikat, rkomunikat, OFFSET liczba,0
	jmp	Pentluj

main endp
atoi proc uses esi edx inputBuffAddr:DWORD
	mov esi, inputBuffAddr
	xor edx, edx
	xor EAX, EAX
	mov AL, BYTE PTR [esi]
	cmp eax, 2dh
	je parseNegative

	.Repeat
		
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0
	mov EAX, EDX
	jmp endatoi

	parseNegative:
	inc esi
	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0

	xor EAX,EAX
	sub EAX, EDX
	jmp endatoi

	endatoi:
	ret
atoi endp
END